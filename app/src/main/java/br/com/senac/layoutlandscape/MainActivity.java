package br.com.senac.layoutlandscape;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textViewTexto;
    private Button buttonOK;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewTexto = (TextView) findViewById(R.id.txtTexto);
        buttonOK = (Button) findViewById(R.id.btnOK);

        Log.i("#LAND", textViewTexto.getText().toString());

        if (buttonOK != null) {
            buttonOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    textViewTexto.setText("OLA");

                }
            });
        }


    }
}
